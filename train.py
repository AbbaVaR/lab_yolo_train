from ultralytics import YOLO

if __name__ == "__main__":
    model = YOLO("yolov8m.yaml")
    model.train(data='./edu_train.yaml', epochs=30, batch=12, device=0, imgsz=640, patience=5,
                name='yolov8m', v5loader=True)
